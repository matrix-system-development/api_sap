<?php

use Illuminate\Http\Request;
use App\Http\Controllers\{ApiController};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('api')->group(function() {
    Route::get('/inq-billing/{param1}/{date}', [ApiController::class, 'DGN_API_BILLING']);
    Route::get('/inq-billing-corp/{param1}/{date}', [ApiController::class, 'DGN_API_BILLING_CORP']);
    Route::get('/inq-payment/{param1}', [ApiController::class, 'DGN_API_CHECKPAYMENT']);
    Route::get('/inq-bp/{param1}', [ApiController::class, 'DGN_API_CUSTOMER']);
    Route::get('/inq-bp-all/', [ApiController::class, 'DGN_API_CUSTOMER_ALL']);
    Route::post('/inq-so', [ApiController::class, 'DGN_API_SALESORDER']);
    Route::get('/inq-ec/{param1}/{param2}', [ApiController::class, 'DGN_API_EQUIP']);
    Route::get('/inq-purchase/{param1}/{date}', [ApiController::class, 'DGN_API_PURCHASE']);
    Route::get('/inq-billing-docnum/{param1}', [ApiController::class, 'DGN_API_BILLING_DOCNUM']);
});
