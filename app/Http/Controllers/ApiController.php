<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function DGN_API_BILLING($param1,$date)
    {
        $query = DB::select('exec DGN_API_BILLING ?,?', array($param1, $date));
        return response()->json([
            'success' => true,
            'row_count' => count($query),
            'results' => $query,
        ],200);
   
    }

    public function DGN_API_BILLING_DOCNUM($param1)
    {
        $query = DB::select('exec DGN_API_BILLING_DOCNUM ?', array($param1));
        return response()->json([
            'success' => true,
            'row_count' => count($query),
            'results' => $query,
        ],200);
    }

    public function DGN_API_BILLING_CORP($param1,$date)
    {
        $query = DB::select('exec DGN_API_BILLING_NON09 ?,?', array($param1, $date));
        return response()->json([
            'success' => true,
            'row_count' => count($query),
            'results' => $query,
        ],200);
   
    }

    public function DGN_API_CHECKPAYMENT($param1)
    {
        $query = DB::select('exec DGN_API_CHECKPAYMENT ?', array($param1));
        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Invoice Number '.$param1.' not found',
            ],200); 
        } else {
            return response()->json([
                'success' => true,
                'results' => $query,
            ],200); 
        }
    }

    public function DGN_API_CUSTOMER($param1)
    {
        $query = DB::select('exec DGN_API_CUSTOMER ?', array($param1));
        
        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Customer '.$param1.' not found',
            ],200); 
        } else {
            return response()->json([
                'success' => true,
                'results' => $query,
            ],200); 
        }
    }

    //DGN CUSTOMER ALL JANGAN DIPAKE DULU PERLU DI OPTIMIZE
    public function DGN_API_CUSTOMER_ALL()
    {
        $query = DB::select('exec DGN_API_CUSTOMER \'\'');
        
        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Inquiry Failed',
            ],200); 
        } else {
            return response()->json([
                'success' => true,
                'row count' => count($query),
                'results' => $query,
            ],200); 
        }
    }

    public function DGN_API_SALESORDER(Request $request)
    {
        //param 1 SO (kalau mau keluar semua data pakai -1)
        //param 2 custcode (bisa di kosongin)
        //param 3 circuit id groupcode (bisa di kosongin)

        $param1 = $request->param1;
        $param2 = $request->param2 ?? '';
        $param3 = $request->param3 ?? '';

        $query = DB::select('exec DGN_API_SALESORDER_ACTIVE ?,?,?', array($param1, $param2, $param3));
        
        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Sales Order for customer '.$param2.' not found',
            ],200); 
        } else {
            return response()->json([
                'success' => true,
                'row count' => count($query),
                'results' => $query,
            ],200); 
        }
    }

    public function DGN_API_EQUIP($param1,$param2)
    {
        $query = DB::select('exec DGN_API_EQUIP ?,?', array($param1, $param2));

        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Equipment Card for customer '.$param2.' not found',
            ],200); 
        } else {
            return response()->json([
                'success' => true,
                'row count' => count($query),
                'results' => $query,
            ],200); 
        }
    }

    public function DGN_API_PURCHASE($param1,$date)
    {
        //dd('hai');
        $query = DB::select('exec DGN_API_PURCHASE ?,?', array($param1, $date));
        return response()->json([
            'success' => true,
            'row_count' => count($query),
            'results' => $query,
        ],200);
    }
}
